FROM node:12.16.1-alpine

WORKDIR /app

COPY . .

RUN apk add --no-cache \
        libmount \
        ttf-dejavu \
        ttf-droid \
        ttf-freefont \
        ttf-liberation \
        ttf-ubuntu-font-family \
        fontconfig \
    && apk add --no-cache --virtual build-deps \
        bash \
        make \
        python \
        g++ \
        cairo-dev \
        jpeg-dev \
        pango-dev \
        pango \
        musl-dev \
        giflib-dev \
        pixman-dev \
        pangomm-dev \
        libjpeg-turbo-dev \
        freetype-dev \
    && npm ci \
    && npm install -g typescript \
    && tsc \
    && apk del build-deps \
    && apk add --no-cache \
        cairo \
        jpeg \
        pango \
        pango-dev \
        musl \
        giflib \
        pixman \
        pangomm \
        libjpeg-turbo \
        freetype \
    && fc-cache -fv

CMD [ "npm", "start" ]