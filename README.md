# Discord Usage Stats

![screenshot](./docs/screenshot.png)

## About

Discord Usage Stats is a Discord bot that collects and displays a server's usage statistics in a graph.

The data collected is total users, users online and users on specific voice channels.

## Development

The server code is running in Typescript. To install do the following commands:

```bash
git clone https://gitlab.com/xenomer/discordusagestats.git
cd discordusagestats
npm i
npm run watch
# in another terminal:
npm run dev
```

## Environment variables

    BOT_TOKEN=<Discord bot token>
    SERVER_NAME=<The name of this server>
    MORE_READABLE_LOGS=<true>
    DB_ADDR=<address to MongoDB server>:27017
    DB_NAME=<name of the database>
    DB_USER=<MongoDB user name>
    DB_PASS=<MongoDB user pass>

## Incomplete stuff

TODO document and comment all the files
( `index.ts` and `database.ts` are already documented ) \
TODO data breakpoints (sends a notification when a node has surpassed a custom-specified treshold) \
