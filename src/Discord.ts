import Logger from "./logger";
import discord, { MessageEmbed } from "discord.js";
import incommand from "in-command";
import database from "./database";
import commands from "./commands";
import { DbGuildData, Contexts } from "./types";
import { FilterQuery } from "mongodb";
import { scheduleSession } from "./guild/scheduler";
import {
  EmbedMessageOptions
} from "./types";
import { ObjectId } from "bson";

export const DEFAULT_COMMAND_PREFIX: string = "d! ";
console.log(DEFAULT_COMMAND_PREFIX);

export let commandPrefixes: {
  [gId: string]: string;
} = {};

class GlobalDiscordInstance {
  public logger = new Logger("discord");
  public client = new discord.Client();
  public startTime = new Date();
  public contexts: Contexts = {};

  initialize() {
    this.logger.info("initializing");
    this.logger.info(
      "command prefix is",
      this.logger.inspect(DEFAULT_COMMAND_PREFIX)
    );
    this.client.on("ready", this.bot_ready.bind(this));
    this.client.on("message", this.bot_message.bind(this));
  }

  login(token: string) {
    this.logger.debug("logging in");
    this.client.login(token);
  }

  embeddedMessage(message: string, options?: EmbedMessageOptions) {
    options = options || {};
    return {
      embed: new discord.MessageEmbed({
        description:
          (options.style === "error"
            ? ":x:  "
            : options.style === "success"
            ? ":white_check_mark:  "
            : options.style === "info"
            ? ":information_source:  "
            : options.style === "question"
            ? ":grey_question:  "
            : "") + message
      })
    };
  }

  usage(prefix: string, command: string, desc: string) {
    return {
      embed: new discord.MessageEmbed({
        title: 'Usage',
        description: `\`${prefix}${command}\`\n` + desc,
      })
    };
  }

  /**
   * Returns the guild's id regardless of whether it
   * is a discord.js guild or DbGuildData object
   *
   * @param {(discord.Guild | DbGuildData)} guild
   * @returns {string}
   * @memberof GlobalDiscordInstance
   */
  typeGetId(guild: discord.Guild | DbGuildData): string {
    if((guild as DbGuildData)._id)
      return (guild as DbGuildData)._id;
    else return (guild as discord.Guild).id;
  }

  private async bot_ready() {
    this.logger.fndebug();

    if (this.client.user) {
      this.logger.info("logged in as", this.client.user.tag);

      let guilds = await database.db
        .collection("guilds")
        .find<DbGuildData>({
          $and: [
            {
              serverOverride: {
                $in: [null, database.name]
              }
            },
            {
              "service.enabled": true,
              "service.hostServer": (await database.getServerInstance())._id
            }
          ]
        } as FilterQuery<DbGuildData>)
        .toArray();

      for (let guild of guilds) {
        this.logger.info("resuming service on guild", guild.name, `(interval ${guild.service.interval}min)`);

        scheduleSession(
          guild._id,
          guild,
          guild.service.currentSession as ObjectId,
          true
        );
      }
    } else {
      this.logger.error("Could not log in!");
    }
  }

  private async bot_message(msg: discord.Message) {
    let guild: discord.Guild;
    let dbGuild: DbGuildData = (null as unknown) as DbGuildData;

    // the message is from us
    if (this.client && msg.author.id === (this.client.user as discord.User).id)
      return;

    if (!msg.guild) {
      this.logger.fndebug(
        [msg.author.tag, msg.content.substring(0, 50)],
        "tried to send a private message"
      );
      msg.channel.send({ content: "Private messages are not supported." });
      return;
    } else {
      guild = msg.guild as discord.Guild;
    }

    // acquire of fetch guild's prefix or
    // default to new if not found
    let guildPrefix = commandPrefixes[guild.id];
    if (!guildPrefix) {
      dbGuild = (await database.getGuild(guild.id, guild)) as DbGuildData;
      if (dbGuild && dbGuild.commandPrefix) {
        guildPrefix = dbGuild.commandPrefix;
        commandPrefixes[guild.id] = guildPrefix;
      } else {
        guildPrefix = DEFAULT_COMMAND_PREFIX;
      }
    }

    // commands
    if (msg.content.startsWith(guildPrefix)) {
      if (!dbGuild) {
        dbGuild = (await database.getGuild(guild.id, guild)) as DbGuildData;
      }
      dbGuild = dbGuild as DbGuildData;

      if (
        !dbGuild ||
        (dbGuild.serverOverride !== null &&
          dbGuild.serverOverride !== database.name)
      ) {
        this.logger.debug(
          "either guild could not be retrieved or this server is being overridden in this guild. ignoring message."
        );
        return;
      }

      let ableToManage = 
        msg.member?.hasPermission("ADMINISTRATOR") ||
        msg.member?.roles.cache.some(r => dbGuild.roles.manage.includes(r.id)) ||
        false;

      let ableToView =
        ableToManage ||
        msg.member?.roles.cache.some(r => dbGuild.roles.view.includes(r.id)) ||
        false;


      msg.content = msg.content.substring(DEFAULT_COMMAND_PREFIX.length);

      let p = function(c: any) {
        return c
          .parseOnExit(false)
          .exitOnHelp(false)
          .exitOnVersion(false)
          // .disableOptions()
          .disableMissingRequiredMessage()
          .disableHelpUsage()
          .disableHelpCommands()
          .disableHelpOptions()
          .disableHelpDescription()
          .disableInteractivity()
          .defaultCommand("unknown")
      }

      let parser = p(incommand)
        .command("info", "", async () => {
          commands.needsToBe(ableToView).run("info", msg, guild, dbGuild);
        })
        .command("start [new]", "", (flags: any, params: any) => {
          commands.needsToBe(ableToManage).run(
            "start",
            msg,
            guild,
            dbGuild,
            !!flags.silent,
            params.new
          );
        })
        .command("stop", "", (flags: any, params: any) => {
          commands.needsToBe(ableToManage).run("stop", msg, guild, dbGuild, !!flags.silent, flags);
        })
        .command("reset", "", (flags: any, params: any) => {
          commands.needsToBe(ableToManage).run("reset", msg, guild, dbGuild, !!flags.silent, flags);
        })
        .command("stats [session]", "", async (flags: any, params: any) => {
          commands.needsToBe(ableToView).run("stats", msg, guild, dbGuild, !!flags.silent, flags, params);
        })
        .command("sessions", "", p(incommand)
          .command('remove [session]', '', async (flags: any, params: any) => {
            if(params.session === undefined && ableToManage) {
              msg.channel.send(this.usage(
                guildPrefix, 'sessions remove [session]', 'Remove a session'
              ))
            } else {
              commands.needsToBe(ableToManage).run("sessions remove", msg, guild, dbGuild, !!flags.silent, flags, params.session);
            }
          })
          .command('unknown', '', async (flags: any, params: any) => {
            commands.needsToBe(ableToView).run("sessions", msg, guild, dbGuild, !!flags.silent, flags);
          }))
        .command("role", "", p(incommand)
          .command('add <level>', '', async (flags: any, params: any) => {
            let role = params._extras?.join(' ') || '';
            if(ableToManage && 
              (!role ||
              params.level === undefined
            )) {
              msg.channel.send(this.usage(
                guildPrefix, 'role add [manage|view] [role]', 'Add a role\'s access for specific level'
              ));
            } else {
              commands.needsToBe(ableToManage)
                .run("role add", msg, guild, dbGuild, !!flags.silent, params.level, role);
            }
          })
          .command('remove <level>', '', async (flags: any, params: any) => {
            let role = params._extras?.join(' ') || '';
            if(ableToManage && 
              (!role ||
              params.level === undefined
            )) {
              msg.channel.send(this.usage(
                guildPrefix, 'role remove [manage|view] [role]', 'Remove a role\'s access from specific level'
              ));
            } else {
              commands.needsToBe(ableToManage)
                .run("role remove", msg, guild, dbGuild, !!flags.silent, params.level, role);
            }
          })
          .command('unknown', '', async (flags: any, params: any) => {
            msg.channel.send(this.usage(
              guildPrefix, 'role [add|remove] [manage|view] [role]', 'Add or remove a role from accessing the bot'
            ));
          }))
        .command("debug", "", async (flags: any, parameters: any) => {
          // TODO we should probably show more useful info and less less useful info
          if(!ableToManage) {
            return;
          }
          let sessionCount = await database.db
            .collection("sessions")
            .countDocuments({ guild: dbGuild._id });
          let nodeCount = await database.db.collection("nodes").countDocuments({
            guild: dbGuild._id
          });

          msg.channel.send({
            embed: new MessageEmbed({
              color: "#7c3186",
              fields: [
                {
                  name: "Sessions",
                  value: sessionCount
                },
                {
                  name: "Total nodes gathered",
                  value: nodeCount
                }
              ]
            })
          });
        })
        .option("silent", "Hide output")
        .parseOnExit(false)
        .exitOnHelp(false)
        .exitOnVersion(false)
        // .disableOptions()
        .disableMissingRequiredMessage()
        .disableHelpUsage()
        .disableHelpCommands()
        .disableHelpOptions()
        .disableHelpDescription()
        .disableInteractivity()
        .defaultCommand("unknown")
        .command("unknown", "", (o: any, p: any) => {
          msg.reply(`Unknown command ${msg.content}`);
        });
      parser.parse(["0", "0", ...msg.content.split(" ")]);
    }
  }
}
var Discord: GlobalDiscordInstance = new GlobalDiscordInstance();

export default Discord;
