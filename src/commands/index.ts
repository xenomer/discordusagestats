import Logger from "../logger";
import discord from 'discord.js';

import info from "./info";
import start from "./start";
import stop from "./stop";
import reset from "./reset";
import stats from "./stats";
import sessions, { remove as sessions_remove } from "./sessions";
import Discord from '../Discord';
import { addRole, removeRole } from './roles';

export const logger = new Logger('commands');

const commands: {[id:string]:any} = {
    info,
    start,
    stop,
    reset,
    stats,
    sessions,
    'sessions remove': sessions_remove,
    'role add': addRole,
    'role remove': removeRole,
}

export interface CommandManager {
    condition?: boolean
    run: (command: string, msg: discord.Message, guild: discord.Guild, ...args: any[]) => this
    needsToBe: (condition: boolean) => this
    noAccess: (msg: discord.Message) => this
}

export default {
    run(command: string, msg: discord.Message, guild: discord.Guild, ...args: any[]) {
        if(this.condition !== undefined &&
            !this.condition) {
            logger.fndebug([guild.id, command], 'no access');
            
            this.noAccess(msg);
            return;
        }
        logger.fndebug([guild.id, command]);
        // logger.debug(msg, guild, ...args);
        if(commands[command]) {
            commands[command](msg, guild, ...args);
        } else {
            logger.fndebug([guild.id, command], 'no such command!');
        }
        return this;
    },
    needsToBe(this: CommandManager, condition: boolean) {
        if(this.condition !== undefined) {
            condition = this.condition && condition
        }
        return {
            ...this,
            condition: condition,
        }
    },
    noAccess(msg: discord.Message) {
        msg.channel.send(Discord.embeddedMessage(
            'Sorry, you don\'t have sufficient ' +
            'permissions to use this command', 
            {
                style: 'error'
            }
        ));
        return this;
    }
} as CommandManager;
