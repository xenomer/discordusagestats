import discord from 'discord.js';
import database from '../database';
import { DbGuildData } from '../types';
import Discord from '../Discord';
import Timespan from 'readable-timespan';

const version = require('../../package').version;


export default function(msg: discord.Message, guild: discord.Guild, dbGuild: DbGuildData) {
    let enabled = dbGuild.service.enabled;
    let uptime = Date.now() - Discord.startTime.valueOf();
    
    msg.channel.send({
    embed: {
        title: 'Bot Information',
        footer: 'Made by Xenomer',
        fields: [
        {
            name: 'Status',
            value: enabled ? 
                ':white_check_mark: Service is currently enabled' :
                ':x: Service is currently disabled',
        },
        // ...(enabled ? [{
        //     name: 'Interval',
        //     value: 'The service is running every ' + dbGuild.service.interval + ' minutes.'
        // }] : []),
        {
            name: 'Bot version',
            value: version,
            inline: true

        },
        {
            name: 'Bot uptime',
            value: new Timespan().parse(uptime),
            inline: true
        },
        {
            name: 'Server name',
            value: database.name
        },
        {
            name: 'Author',
            value: 'Xenomer (Johannes Vääräkangas) :fire:'
        }
        ]
    }
    })
}