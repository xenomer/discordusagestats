import discord from 'discord.js';
import { DbGuildData } from '../types';
import Discord from '../Discord';
import { stopSession, endSession } from '../guild/sessions';
import showStats from './stats';


// thresholds - send message if specific rule matches
// continuous + sessions

/**
 * format: reset [--silent]
 * 
 * Resets the current session (if any) and shows latest data,
 * 
 * @export
 * @param {discord.Message} msg
 * @param {discord.Guild} guild
 * @param {DbGuildData} dbGuild
 * @param {boolean} silent
 * @param {string} interval
 */
export default async function(msg: discord.Message, guild: discord.Guild, dbGuild: DbGuildData, silent: boolean, args: any) {
    let enabled = dbGuild.service.enabled;
    let currentSession = dbGuild.service.currentSession;

    if(enabled) {
        let result = await stopSession(guild, dbGuild, !!args.force);
        if(!silent && !result) {
            msg.channel.send(Discord.embeddedMessage('Could not stop session', { style: 'error' }));
            return;
        }
    }

    if(currentSession) {
        if(!silent) {
            msg.channel.send(Discord.embeddedMessage('Session successfully reset. Last data:', { style: 'success' }));
            await showStats(msg, guild, dbGuild, silent, args, {});
        }

        await endSession(guild, dbGuild);
    } else {
        if(!silent) {
            msg.channel.send(Discord.embeddedMessage('Nothing to reset', { style: 'info' }));
        }
    }
}