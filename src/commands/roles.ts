import discord from 'discord.js';
// import { logger } from './index';
import { DbGuildData } from '../types/database.types';
import Discord from '../Discord';
import database from '../database';

export async function addRole(msg: discord.Message, guild: discord.Guild, dbGuild: DbGuildData, silent: boolean, mode: string, role: string) {
    let guildRole = msg.guild?.roles.cache.find(r => r.name.toLowerCase() === role.toLowerCase());
    mode = mode.toLowerCase();

    if (!guildRole) {
        msg.channel.send(Discord.embeddedMessage('Invalid role ' + role + '!', { style: 'error' }));
    } else {
        if(mode === 'manage' ||
            mode === 'view') {
            let arr = dbGuild.roles[mode];
            if(!arr.includes(guildRole.id)) {
                arr.push(guildRole.id);
                await database.db.collection('guilds').updateOne({
                    _id: dbGuild._id,
                }, {
                    $set: {
                        ['roles.' + mode]: arr
                    }
                });
            }

            msg.channel.send(Discord.embeddedMessage(guildRole.name + ' can now ' +  mode + ' the bot.', { style: 'success' }));
        } else {
            msg.channel.send(Discord.embeddedMessage('Invalid level. Use either `view` or `manage`', { style: 'error' }));
        }
    }
}

export async function removeRole(msg: discord.Message, guild: discord.Guild, dbGuild: DbGuildData, silent: boolean, mode: string, role: string) {
    let guildRole = msg.guild?.roles.cache.find(r => r.name.toLowerCase() === role.toLowerCase());
    mode = mode.toLowerCase();

    if (!guildRole) {
        msg.channel.send(Discord.embeddedMessage('Invalid role ' + role + '!', { style: 'error' }));
    } else {
        if(mode === 'manage' ||
            mode === 'view') {
            let arr = dbGuild.roles[mode];
            if(arr.includes(guildRole.id)) {
                arr.splice(arr.findIndex(id => id === guildRole?.id), 1);
                await database.db.collection('guilds').updateOne({
                    _id: dbGuild._id,
                }, {
                    $set: {
                        ['roles.' + mode]: arr
                    }
                });
            }

            msg.channel.send(Discord.embeddedMessage(guildRole.name + ' can ' +  mode + ' the bot no more.', { style: 'success' }));
        } else {
            msg.channel.send(Discord.embeddedMessage('Invalid level. Use either `view` or `manage`', { style: 'error' }));
        }
    }
}