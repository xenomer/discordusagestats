import discord from 'discord.js';
import { DbGuildData } from '../types';
import Discord from '../Discord';
import database from '../database';
import { findSession, removeSession, endSession } from '../guild/sessions';
import { DbSessionData } from '../types/database.types';

/**
 * format: sessions
 * 
 * Lists sessions
 * 
 * @export
 * @param {discord.Message} msg
 * @param {discord.Guild} guild
 * @param {DbGuildData} dbGuild
 * @param {boolean} silent
 */
export default async function(msg: discord.Message, guild: discord.Guild, dbGuild: DbGuildData, silent: boolean, args: any) {

    // find all sessions in this guild
    let sessions = await database.db.collection('sessions')
        .find({
            guild: dbGuild._id
        })
        .sort({
            started: -1
        })
        .toArray() as DbSessionData[];
    
    // the index used to "name" the sessions, this
    // also works as an identifier for getting stats
    // for and removing a specific session
    let index = 1;

    msg.channel.send({
        embed: new discord.MessageEmbed({
            title: 'Sessions',

            // only show the description if no sessions exist
            description: sessions.length ? '' : 'No sessions',
            
            fields: sessions.length 
                ? sessions.map(session => ({
                    name: `Session **${index++}**`,
                    value: `${session.started.toLocaleString()} - ${session.ended ? session.ended.toLocaleString() : 'just now'}`
            })) : [ ]
        })
    });
    
}

/**
 * format: sessions remove [session]
 * 
 * Removes a specific session
 * 
 * @export
 * @param {discord.Message} msg
 * @param {discord.Guild} guild
 * @param {DbGuildData} dbGuild
 * @param {boolean} silent
 * @param {string} args
 * @param {string} sessionIdentifier
 */
export async function remove(msg: discord.Message, guild: discord.Guild, dbGuild: DbGuildData, silent: boolean, args: any, sessionIdentifier: string) {
    let searchQuery = sessionIdentifier;
    let session: DbSessionData = await findSession(searchQuery, guild) as any;

    // define answer reaction emojis
    let yesEmoji = '✅';
    let noEmoji = '❌';

    if(!session) {
        msg.channel.send(Discord.embeddedMessage('No session found', {style: 'error'}));
        return;
    } else {
        // send confirmation query message
        let msg2 = await msg.channel.send(Discord.embeddedMessage(`Are you sure you want to remove session **${searchQuery}**?`, {style: 'question'}));

        // set the answer reactions
        await msg2.react(yesEmoji);
        await msg2.react(noEmoji);

        // create the reaction collector. This will
        // collect a maximum of 1 reaction matching
        // the filter specified in the 1st argument.
        let collector = msg2.createReactionCollector((reaction, user) => {
            return user.id === msg.author.id && (
                reaction.emoji.name === yesEmoji ||
                reaction.emoji.name === noEmoji
            );
        }, {
            max: 1,      // maximum reactions to collect
            time: 60_000 // timeout of 60s, after this
                         // the process will be aborted.
        });

        // delete the confirmation message when we are
        // done waiting for reactions.
        let end = () => { msg2.delete(); }
        collector.on('end', end);

        // this is called when a reaction is registered.
        // Note that this only collects the ones going
        // through the filter.
        collector.on('collect', async r => {
            // whether to remove or abort
            let remove = r.emoji.name === yesEmoji;

            // stop the collector 
            //? (this might actually
            //? be unnecessary since we are collecting
            //? only a max of one reaction anyway.)
            collector.stop();

            if(!remove) {
                // abort
                msg.channel.send(Discord.embeddedMessage(`Aborted session deletion`, {style: 'error'}));
            } else {
                // since the reaction can come anytime and
                // additional changes to the state may have
                // been done, we should query the newest data
                let db = await database.getGuild(dbGuild._id) as DbGuildData;


                if(db.service.enabled && db.service.currentSession === session._id) {
                    msg.channel.send(Discord.embeddedMessage(`Cannot remove ongoing session`, {style: 'error'}));
                    return;
                } else if (db.service.currentSession === session._id) {
                    // if a session is not ongoing but current (therefore a
                    // `start` command simply continues this session),
                    // end the session which removes it from this role
                    await endSession(guild, db);
                }

                // now we can actually remove the session
                await removeSession(db, session._id);
                
                msg.channel.send(Discord.embeddedMessage(`Successfully deleted session`, {style: 'success'}));
            }
        });

    }
}