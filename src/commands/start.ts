import discord from 'discord.js';
import database from '../database';
import { DbGuildData } from '../types';
// import schedule from 'node-schedule';
import Discord from '../Discord';
import { startSession } from '../guild/sessions';
import { ObjectId } from 'bson';


// TODO thresholds - send message if specific rule matches
// TODO continuous + sessions
// TODO user permission system

/**
 * format: start [new] [--silent]
 * 
 * Starts or continues the service depending on whether a sessions exists.
 * If ´new´ is specified, starts a new session and moves current session
 * to past sessions.
 * 
 * @export
 * @param {discord.Message} msg
 * @param {discord.Guild} guild
 * @param {DbGuildData} dbGuild
 * @param {boolean} silent
 * @param {string} interval
 */
export default async function(msg: discord.Message, guild: discord.Guild, dbGuild: DbGuildData, silent: boolean, subCommand?: string) {
    let enabled = dbGuild.service.enabled; // Object.keys(timers).includes(guild.id)

    if(!enabled) {
        let result = await startSession(guild, dbGuild, subCommand === 'new');
        if(!silent) {
            if(result) {
                msg.channel.send(Discord.embeddedMessage('Session ' + (result === 'new' ? 'started' : 'continued'), { style: 'success' }));
            } else {
                msg.channel.send(Discord.embeddedMessage('Could not start session', { style: 'error' }));
            }
        }
    } else {
        if(!silent) {
            let hostServer = dbGuild.service.hostServer as ObjectId;
            let thisServer = (await database.getServerInstance())._id;
            let isDifferentServerRunning = !thisServer.equals(hostServer);

            if(isDifferentServerRunning) {
                msg.channel.send(Discord.embeddedMessage('Service is already running but it\' on a different server', { style: 'error' }));
            } else {
                msg.channel.send(Discord.embeddedMessage('Service is already running', { style: 'error' }));
            }
        }
    }
}