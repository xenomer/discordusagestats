import discord from 'discord.js';
import { DbGuildData } from '../types';
import Discord from '../Discord';
import { findSession, fetchNodes } from '../guild/sessions';
import statistics from '../statistics';
import util from 'util';


// thresholds - send message if specific rule matches
// continuous + sessions

/**
 * format: reset [--silent]
 * 
 * Resets the current session (if any) and shows latest data,
 * 
 * @export
 * @param {discord.Message} msg
 * @param {discord.Guild} guild
 * @param {DbGuildData} dbGuild
 * @param {boolean} silent
 * @param {string} interval
 */
export default async function(msg: discord.Message, guild: discord.Guild, dbGuild: DbGuildData, silent: boolean, args: any, cmd: any) {
    let searchQuery = cmd.session || '1';
    let session = await findSession(searchQuery, guild);

    msg.channel.send(Discord.embeddedMessage(util.inspect(session), {style: 'error', timestamp: true}));

    if(!session) {
        msg.channel.send(Discord.embeddedMessage('No session found', {style: 'error', timestamp: true}));
        return;
    } else {
        let nodes = await fetchNodes(session);

        if(nodes.length < 2) {
            msg.channel.send({
                embed: {
                    title: 'Statistics',
                    timestamp: new Date().valueOf(),
                    footer: {
                        text: 'provided by The Almighty Xenomer'
                    },
                    description: 'Not enough data yet. Check back later!'
                } as discord.MessageEmbed
            });
            return;
        }

        let stats = await statistics(nodes, guild, session);

        msg.channel.send({
            embed: {
                title: 'Statistics',
                timestamp: new Date().valueOf(),
                footer: {
                    text: 'provided by The Almighty Xenomer'
                },
                fields: [
                    {
                        name: 'Maximum users online',
                        value: Math.round(stats.max).toString() + " users",
                        inline: true,
                    },
                    {
                        name: 'Minimum users online',
                        value: Math.round(stats.min).toString() + " users",
                        inline: true,
                    },
                    {
                        name: 'Average users online',
                        value: (Math.round(stats.average * 100) / 100).toString() + " users",
                        inline: true,
                    },
                    {
                        name: 'Maximum users in voice channels',
                        value: Math.round(stats.voicechannels_max).toString() + " users",
                    },
                    {
                        name: 'Average users in voice channels',
                        value: (Math.round(stats.voicechannels_average * 100) / 100).toString() + " users"
                    }
                ]
            } as discord.MessageEmbed,
            files: [ stats.chart ]
        });

        // remove the temp file after timeout
        setTimeout(stats.close, 5000)

        // msg.channel.send(Discord.embeddedMessage(util.inspect(nodes), { timestamp: true }));
    }
}
