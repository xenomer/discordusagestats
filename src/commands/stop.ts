import discord from 'discord.js';
import { DbGuildData } from '../types';
import Discord from '../Discord';
import { stopSession } from '../guild/sessions';


// thresholds - send message if specific rule matches
// continuous + sessions

/**
 * format: start [new] [--silent]
 * 
 * Starts or continues the service depending on whether a sessions exists.
 * If ´new´ is specified, starts a new session and moves current session
 * to past sessions.
 * 
 * @export
 * @param {discord.Message} msg
 * @param {discord.Guild} guild
 * @param {DbGuildData} dbGuild
 * @param {boolean} silent
 * @param {string} interval
 */
export default async function(msg: discord.Message, guild: discord.Guild, dbGuild: DbGuildData, silent: boolean, args: any) {
    let enabled = dbGuild.service.enabled;

    if(enabled) {
        let result = await stopSession(guild, dbGuild, !!args.force);
        if(!silent) {
            if(result) {
                msg.channel.send(Discord.embeddedMessage('Session stopped', { style: 'success' }));
            } else {
                msg.channel.send(Discord.embeddedMessage('Could not stop session', { style: 'error' }));
            }
        }
    } else if(!silent) {
        msg.channel.send(Discord.embeddedMessage('No session is currently ongoing', { style: 'error' }));
    }
}