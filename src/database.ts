import { MongoClient, Db, FilterQuery, UpdateQuery, ObjectId } from 'mongodb';
import Logger, { setGlobalLogLevel } from './logger';
import discord from 'discord.js';
import { DEFAULT_COMMAND_PREFIX } from './Discord';
import { DbServerData, DbGuildData, DbSessionData, DbNodeData } from './types';

let logger: Logger;

// a snapshot of the database for
// advanced use by outside logic
let db: Db | null = null;

// a snapshot of this server's name
// (the SERVER_NAME environment
// variable if it exists)
let serverName = 'default';

// the default layout for a guild in the database
export const defaultGuildLayout: Partial<DbGuildData> = {
    roles: {
        view: [],
        manage: []
    },
    serverOverride: null,
    sessions: { },
    service: {
        enabled: false,
        interval: 5,
        hostServer: null,
        currentSession: null,
        cleanThreshold: 604_800_000 // 1 week in ms
    },
}

export default {
    // we disregard the sanitycheck of
    // db possibly being null because
    // if we disconnect from the database
    // a hard crash is well deserved.
    get db(): Db { return db as Db; },

    get name(): string { return serverName; },
    
    /**
     * Initializes and connects to the MongoDB database. If we don't have a
     * server instance document in there, create it and all the required collections
     *  
     *  @returns {Promise<boolean>} whether the connection and initialization was successful
     */
    async init(): Promise<boolean> {
        logger = new Logger('mongodb');

        // check for required env variables
        if(!process.env.DB_USER || !process.env.DB_PASS) {
            logger.error('Database credentials DB_USER or DB_PASS were not provided!');
            return false;
        }

        if(!process.env.DB_NAME) {
            logger.info('DB_NAME was not provided; using default \'discordusagestats\'');
        }

        let addr = process.env.DB_ADDR || 'mongodb://localhost:27017';
        let dbName = process.env.DB_NAME || 'discordusagestats';
        let user = encodeURIComponent(process.env.DB_USER);
        let pass = encodeURIComponent(process.env.DB_PASS);

        // authentication mechanism to use
        const authMechanism = 'SCRAM-SHA-1';
        logger.debug('using authentication mechanism', authMechanism);

        const url = `mongodb://${user}:${pass}@${addr}/?authMechanism=${authMechanism}&authSource=${dbName}`;
        logger.debug('connection url:', url);

        const client = new MongoClient(url, {
            useUnifiedTopology: true
        });


        // try to connect to the database. If we fail,
        // return a failure.
        try {
            await client.connect();
        } catch(e) {
            logger.error('Could not connect to database:', e);
            return false;
        }

        // yayy, connection successful!
        logger.info('connected to database!')

        // initialize the variables
        serverName = process.env.SERVER_NAME || 'default';
        db = client.db(dbName);

        // get all the matching instances
        let instance: DbServerData | null = await db.collection('servers')
            .findOne({ name: serverName });
        
        if(!instance) {
            logger.info('creating new server instance');
            instance = {
                _id: new ObjectId(),
                name: serverName,
                created: new Date().toISOString(),
                debug: false,
            };

            // insert our new server instance back to the db
            await db.collection('servers')
                .insertOne(instance);
        }

        // if we have the debug flag enabled,
        // enable global debugging log level
        if(instance.debug) {
            setGlobalLogLevel(0);
        }

        // display some cool data
        logger.debug('server instance:', instance);
        logger.info('server information:');
        logger.info(' - name:', instance.name);
        logger.info(' - created:', instance.created);
        logger.info(' - debug:', instance.debug);

        return true;
    },

    /**
     * Returns a guild if it exists. If it does not exist, this actually
     * creates one automaticallu (assuming a discord guild is provided)
     * and returns that instead.
     *
     * @param       {string}                                guildId
     * @param       {discord.Guild}                         [guild]
     * @returns     {(Promise<DbGuildData | null>)}         The guild or null if one could not be created
     */
    async getGuild(guildId: string, guild?: discord.Guild): Promise<DbGuildData | null> {
        if(!db) {
            logger.fndebug(guildId, 'not connected')
            logger.error('error: not connected to database!');
            return null;
        } else {
            // search for the guild
            let found = await db.collection('guilds')
                .findOne({ _id: guildId });
            
            if(!found) {
                logger.fndebug(guildId, 'guild not found')
                if(guild) {
                    // try to create the guild
                    return await this.createGuild(guildId, guild);
                } else {
                    return null;
                }
            } else {
                return found;
            }
            
        }
    },

    /**
     * Creates a new guild in the database
     *
     * @param       {string}                            guildId
     * @param       {discord.Guild}                     guild
     * @returns     {(Promise<DbGuildData | null>)}     The created guild or null if one could not be created
     */
    async createGuild(guildId: string, guild: discord.Guild): Promise<DbGuildData | null> {
        if(!db) {
            logger.fndebug(guildId, 'not connected')
            logger.error('error: not connected to database!');
            return null;
        } else {
            // create the object
            let _guild = {
                _id: guild.id,
                name: guild.name,
                created: new Date().toISOString(),
                commandPrefix: DEFAULT_COMMAND_PREFIX,
                ...defaultGuildLayout
            } as DbGuildData;

            // insert it into the database
            await db.collection('guilds')
                .insertOne(_guild);

            logger.fndebug(guildId, _guild);
            logger.info('created new guild', guildId);

            return _guild;
        }
    },

    /**
     * Returns this server's server instance
     *
     * @returns {Promise<DbServerData>}
     */
    async getServerInstance(): Promise<DbServerData> {
        if(!db) {
            logger.fndebug('', 'not connected to database');
            logger.error('error: not connected to database!');
            return null as unknown as DbServerData;
        } else {
            return await db.collection('servers').findOne({ name: this.name }) as DbServerData;
        }
    },

    /**
     * A shorthand for db.collection('guilds').updateOne(...)
     *
     * @param {FilterQuery<DbGuildData>}                            filter
     * @param {(UpdateQuery<DbGuildData> | Partial<DbGuildData>)}   document
     * @returns
     */
    async updateGuild(filter: FilterQuery<DbGuildData>, document: UpdateQuery<DbGuildData> | Partial<DbGuildData>) {
        if(!db) {
            logger.fndebug('', 'not connected to database');
            logger.error('error: not connected to database!');
            return null;
        } else {
            return await db.collection('guilds').updateOne(filter, document);
        }
    },

    /**
     * A shorthand for db.collection('sessions').findOne(...)
     *
     * @param {FilterQuery<DbSessionData>}          filter
     * @returns {(Promise<DbSessionData | null>)}
     */
    async getSession(filter: FilterQuery<DbSessionData>): Promise<DbSessionData | null> {
        if(!db) {
            logger.fndebug('', 'not connected to database');
            logger.error('error: not connected to database!');
            return null;
        } else {
            return await db.collection('sessions').findOne(filter);
        }
    },

    /**
     * A shorthand for db.collection('sessions').updateOne(...)
     *
     * @param {FilterQuery<DbSessionData>}                              filter
     * @param {(UpdateQuery<DbSessionData> | Partial<DbSessionData>)}   document
     * @returns
     */
    async updateSession(filter: FilterQuery<DbSessionData>, document: UpdateQuery<DbSessionData> | Partial<DbSessionData>) {
        if(!db) {
            logger.fndebug('', 'not connected to database');
            logger.error('error: not connected to database!');
            return null;
        } else {
            return await db.collection('sessions').updateOne(filter, document, {
                upsert: true
            });
        }
    },

    /**
     * A shorthand for db.collection('nodes').insertOne(...)
     *
     * @param {(UpdateQuery<DbNodeData> | Partial<DbNodeData>)} document
     * @returns
     */
    async insertNode(document: UpdateQuery<DbNodeData> | Partial<DbNodeData>) {
        if(!db) {
            logger.fndebug('', 'not connected to database');
            logger.error('error: not connected to database!');
            return null;
        } else {
            return await db.collection('nodes').insertOne(document);
        }
    }
}