import discord from "discord.js";
import { DbVoiceChannelData, DbGuildData, DbNodeData } from '../types';
import Logger from "../logger";
import { ObjectId } from "bson";

let logger: Logger = new Logger("analyzer");

export function gatherGuildNode(dbGuild: DbGuildData, guild: discord.Guild): Partial<DbNodeData> {
    logger.fndebug(dbGuild._id);

    if (guild.available) {
        // count presences
        let online = 0;
        let total = 0;

        guild.members.cache.forEach(p => {
            total++;
            if (["online", "dnd", "idle"].includes(p.presence.status)) {
                online++;
            }
        });

        let channels: DbVoiceChannelData[] = [];

        guild.channels.cache.forEach((channel: discord.Channel) => {
            if (channel.type === "voice") {
                let voicechannel = channel as discord.VoiceChannel;
                let usersOnChannel = (channel as discord.VoiceChannel).members.size;
                let limit = (channel as discord.VoiceChannel).userLimit;
                channels.push({
                    name: voicechannel.name,
                    users: usersOnChannel,
                    maxUsers: limit || null // 0 -> null
                });
            }
        });
        if (guild.channels.cache.size === 0) {
        }

        return {
            _id: new ObjectId(),
            timestamp: new Date(),
            guild: guild.id,
            totalUsers: total,
            onlineUsers: online,
            voiceChannels: channels
        };
    } else {
        logger.debug(`- not available`);
        return { };
    }
}