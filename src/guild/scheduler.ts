import Logger from '../logger';
import { DbGuildData, DbNodeData } from '../types';
import schedule from 'node-schedule';
import database from '../database';
import { gatherGuildNode } from './analyzer';
import Discord from '../Discord';
import { ObjectId } from 'mongodb';

let logger: Logger = new Logger('scheduler');

export function scheduleSession(guildId: string, dbGuild: DbGuildData, sessionId: ObjectId, scheduleNow?: boolean) {
    logger.fndebug([guildId]);

    let cron = _buildCronString(dbGuild.service.interval);
    
    // the schedule id is of format `guildId.current`
    schedule.scheduleJob(sessionId + '.current', cron, () => _doSchedule(guildId));

    if(scheduleNow) {
        _doSchedule(guildId);
    }
}

export function unscheduleSession(sessionId: ObjectId) {
    schedule.cancelJob(sessionId + '.current');
    logger.fndebug(sessionId, 'unsheduled');
}

export function rescheduleSession(sessionId: ObjectId, interval: number) {
    schedule.rescheduleJob(sessionId + '.current', _buildCronString(interval));
    logger.fndebug(sessionId, ['rescheduled to', interval, 'mins']);
}

//#region private
function _buildCronString(interval: number): string {
    return `*/${interval} * * * *`;
}

async function _doSchedule(guildId: string) {
    logger.fndebug(guildId);

    let guild = await database.getGuild(guildId);
    if(!guild) {
        logger.error('guild', guildId, 'does not seem to exist');
        return;
    } else if (!guild.service.currentSession) {
        logger.error('guild', guildId, 'does not have an active session');
        return;
    }
    let dGuild = Discord.client.guilds.resolve(guild._id);

    let session = await database.getSession({
        _id: guild.service.currentSession
    });
    if(!dGuild) {
        logger.error('guild', guildId, 'does not seem to exist in discord');
        return;
    } else if (!session) {
        logger.error('session', guild.service.currentSession, 'does not seem to exist');
        return;
    }
    
    let node = gatherGuildNode(guild, dGuild);
    if(!node) {
        return;
    } else {
        await database.insertNode({
            session: session._id,
            ...node
        } as DbNodeData);
        logger.fndebug([guildId, session._id], 'inserted new node');
        await _doTrimOldNodes(session._id, guild);
    }
}
async function _doTrimOldNodes(sessionId: ObjectId, dbGuild: DbGuildData) {
    let cleanTreshold = dbGuild.service.cleanThreshold;
    let date = new Date(Date.now() - cleanTreshold);
    logger.debug('cleaning nodes earlier than', date.toISOString());

    let result = await database.db
        .collection('nodes')
        .deleteMany({
            timestamp: {
                $lt: date
            },
            session: sessionId,
        });
    logger.debug('cleaned', result.deletedCount, 'nodes');
}
//#endregion