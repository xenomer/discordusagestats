import discord from 'discord.js';
import database from '../database';
import { DbGuildData, DbSessionData, DbNodeData } from '../types';
import Logger from '../logger';
import { scheduleSession, unscheduleSession } from './scheduler';
import { ObjectId } from 'bson';
import Discord from '../Discord';

let logger: Logger = new Logger('sessions');

export async function startSession(guild: discord.Guild, dbGuild: DbGuildData, forceNew: boolean): Promise<false | 'new' | 'continue'> {
    logger.fndebug(guild.id);
    logger.info('starting session for guild', guild.id, guild.name);
    logger.info(' - interval:', dbGuild.service.interval, 'min');

    if(dbGuild.service.hostServer !== null || dbGuild.service.enabled === true) {
        logger.error('session is already ongoing!');
        return false;
    }

    let serverInstance = await database.getServerInstance();

    //update database
    await database.updateGuild({ _id: guild.id }, {
        $set: {
            "service.enabled": true,
            "service.hostServer": serverInstance._id
        }
    });

    let sessionId: ObjectId;

    if(forceNew && dbGuild.service.currentSession) {
        await endSession(guild, dbGuild);
    }

    let continued;
    if(!dbGuild.service.currentSession) {
        logger.debug('starting new session');
        continued = false;
        sessionId = new ObjectId();

        await createSession(guild.id, sessionId);
        await database.updateGuild({ _id: guild.id }, {
            $set: {
                "service.currentSession": sessionId
            }
        });
    } else {
        sessionId = dbGuild.service.currentSession;
        continued = true;
    }

    scheduleSession(guild.id, await database.getGuild(guild.id) as DbGuildData, sessionId);
    return continued ? 'continue' : 'new';
}

export async function stopSession(guild: discord.Guild, dbGuild: DbGuildData, force: boolean): Promise<boolean> {
    logger.fndebug(guild.id);
    logger.info('stopping session for guild', guild.id)

    if(dbGuild.service.hostServer === null || dbGuild.service.enabled === false || dbGuild.service.currentSession === null) {
        logger.error('session is not either ongoing or does not have an assigned server');
        return false;
    }

    let serverInstance = await database.getServerInstance();

    if(!dbGuild.service.hostServer.equals(serverInstance._id) && !force) {
        logger.error('session does not belong to this server');
        return false;
    }

    //update database
    await database.updateGuild({ _id: guild.id }, {
        $set: {
            "service.enabled": false,
            "service.hostServer": null
        }
    });

    unscheduleSession(dbGuild.service.currentSession);
    return true;
}

export async function endSession(guild: discord.Guild, dbGuild: DbGuildData) {
    if(dbGuild.service.currentSession) {
        let session = dbGuild.service.currentSession;
        logger.fndebug([guild.id, session]);
        logger.debug('ending session', session);

        await database.updateSession({ _id: session }, {
            $set: {
                ended: new Date()
            }
        });
        await database.updateGuild({ _id: dbGuild._id }, {
            $set: {
                "service.currentSession": null
            }
        });
        dbGuild.service.currentSession = null;
    }
}

export async function removeSession(guild: DbGuildData, session: ObjectId) {
    logger.fndebug([guild._id, session.toHexString()]);
    if(guild.sessions[session.toHexString()]) {
        logger.fndebug([guild._id, session.toHexString()], 'deleting');
        await database.db.collection('guilds').updateOne({
                _id: guild._id
            }, {
                $unset: {
                    ['services.' + session.toHexString()]: ''
                }
            });
        await database.db.collection('sessions').deleteOne({
            _id: session
        });
        await database.db.collection('nodes').deleteOne({
            session: session
        });
    }
}

export async function createSession(guildId: string, sessionId: ObjectId, name?: string) {
    const created = new Date();
    name = name || created.toISOString();
    database.updateGuild({
        _id: guildId
    }, {
        $set: {
            ["sessions." + sessionId]: {
                _id: sessionId,
                name: name,
                started: created
            }
        }
    });
    database.updateSession({
        _id: sessionId
    }, {
        $set: {
            _id: sessionId,
            ended: null,
            guild: guildId,
            name: name,
            started: created
        }
    })
}

export async function findSession(identifier: string, guild: DbGuildData | discord.Guild): Promise<DbSessionData | null> {
    let id: number | null = null;
    try {
        id = Number.parseInt(identifier);
    } catch(e) {
        logger.debug('could not parse', identifier, 'as a string');
    }
    if(id === null && ![
        'latest', 'new', 'now', 'last',
    ].includes(identifier.toLowerCase())) {
        return null;
    } else if(id === null) {
        id = 1;
    }

    let guildId = Discord.typeGetId(guild);

    let results = await database.db.collection('sessions')
        .find({ 
            guild: guildId,
        })
        .sort({
            started: -1
        })
        .limit(id)
        .toArray();
    if(results.length < (id)) {
        return null;
    } else {
        return results[id - 1];
    }
}

export async function fetchNodes(session: DbSessionData): Promise<DbNodeData[]> {
    let results = await database.db.collection('nodes')
        .find({
            session: session._id
        })
        .sort({
            timestamp: -1
        })
        .toArray();
    return results;
}