import path from 'path';
import fs from 'fs';

export const DATA_FOLDER: string = "data"
export const TEMP_FOLDER: string = "tmp"

if(!fs.existsSync(DATA_FOLDER)) {
    fs.mkdirSync(DATA_FOLDER);
}
if(!fs.existsSync(path.join(DATA_FOLDER, TEMP_FOLDER))) {
    fs.mkdirSync(path.join(DATA_FOLDER, TEMP_FOLDER));
}

export function getTempFilePath(name: string) {
    return path.join(DATA_FOLDER, TEMP_FOLDER, name);
}