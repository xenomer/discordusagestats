import Discord from "./Discord";
import { config } from 'dotenv';
import Logger from './logger';
import database from "./database";

// init dotenv files
config();

let logger = new Logger("main");

export const AppName = 'Discord Usage Stats';


const version = require('../package').version;


logger.info('==========================================');
logger.info(`   ${AppName} v${version}`);
logger.info('   Made by Xenomer (Johannes Vääräkangas)');
logger.info('==========================================');
logger.info();

// initialization sanity check
let _init = false;

// database connection failure reconnect timeout
const reconnect_timeout = 5000;

/**
 * Main initialization logic. 
 * Called first on the server.
 *
 */
async function init(): Promise<void> {
  logger.fndebug();
  if(_init) {
    logger.fatal('already initialized!');
  }
  else {
    _init = true;

    // do we have Discord bot token on
    // environment variables?
    if(process.env.BOT_TOKEN) {

      // try to connect to database
      if(!(await database.init())) {
        // cancel the whole 'initialization' thing for now,
        // otherwise we cannot reinitialize
        _init = false;

        logger.error('reconnecting in', reconnect_timeout, 'ms');
        setTimeout(init, reconnect_timeout);

        return;
      }

      // initialize Discord bot logic
      Discord.initialize();
      Discord.login(process.env.BOT_TOKEN);

    } else {
      logger.fatal('No bot token was provided with BOT_TOKEN.')
      process.exit(1);
      return;
    }
  }
}
init();