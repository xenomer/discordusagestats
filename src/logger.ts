import format from 'string-format';
import util from 'util';
import colors from 'colors/safe';
import Timespan from 'readable-timespan';
import { LogLevel, LogOutput, LogItem } from './types';

// used to display time data
// TODO benchmark and investigate performance
const timespan = new Timespan({
  lessThanFirst: '0ms',
  millisecond: 'ms',
  second: 's',
  minute: 'min',
  hour: 'h',
  day: 'd',
  week: false,
  month: false,
  year: false,
  space: false,
  pluralize: false
});

let globalLogLevel: number = 1;

// used to parse logging level from the LOG_LEVEL
// env variable
if(typeof(process.env.LOG_LEVEL) !== 'undefined') {
  let levels:
    {[id: string]: number} = {
      '0': 0,
      '1': 1,
      '2': 2,
      '3': 3,
      'debug': 0,
      'info': 1,
      'error': 2,
      'fatal': 3,
  }
  if(levels[process.env.LOG_LEVEL.toLowerCase()]
      !== undefined)

    globalLogLevel = 
      levels[process.env.LOG_LEVEL.toLowerCase()];
}

export let moreReadableLogs = false;

if(typeof(process.env.MORE_READABLE_LOGS) !== 'undefined') {
  moreReadableLogs = Boolean(process.env.MORE_READABLE_LOGS);
}

/**
 * Logging format used by string outputs
 */
export const LOG_FORMAT: string = "{date} {modulePad}[{module}]{levelPad}[{level}] {content}"

/**
 * Changes the global logging level (unless overridden)
 * at runtime.
 *
 * @export
 * @param {number} newLevel
 */
export function setGlobalLogLevel(newLevel: number) {
  globalLogLevel = newLevel;
}

/**
 * Used to show lines on the terminal
 *
 * @export
 * @class Logger
 */
export default class Logger {
  /**
   * This logger's name
   *
   * @type {string}
   * @memberof Logger
   */
  name: string;

  /**
   * This logger's outputs
   *
   * @type {LogOutput[]}
   * @memberof Logger
   */
  outputs: LogOutput[];

  /**
   * Whether to override the global log level
   *
   * @private
   * @type {(number | null)}
   * @memberof Logger
   */
  private logLevelOverride: number | null;

  /**
   * Last log time in ms from epoch
   *
   * @private
   * @type {number}
   * @memberof Logger
   */
  private lastLogTime: number = 0;

  /**
   * Last benchmark start
   *
   * @private
   * @type {(number | null)}
   * @memberof Logger
   */
  private lastBenchmark: number | null = null;

  /**
   * This logger's log level. Returns
   * either the global log level or
   * local level if overridden
   *
   * @readonly
   * @type {number}
   * @memberof Logger
   */
  public get logLevel(): number {
    if(this.logLevelOverride !== null)
      return this.logLevelOverride;
    else return globalLogLevel;
  }

  /**
   * Creates an instance of Logger.
   * @param {string}        name        name of this logger
   * @param {LogOutput[]}   outputs     outputs this instance logs to
   * @memberof Logger
   */
  constructor(name: string) {
    this.name = name.toLowerCase();
    this.outputs = [ LogOutput.Console ];
    this.logLevelOverride = null;
  }

  /**
   * Overrides the global log level
   *
   * @param {number} newLogLevel
   * @memberof Logger
   */
  public changeLogLevel(newLogLevel: number): void {
    this.logLevelOverride = Math.min(
      3, Math.max(newLogLevel, 0)
    );
    this.debug('Changed logger level to ')
  }

  /**
   * Converts a string loglevel to corresponding number
   *
   * @private
   * @param {LogLevel} loglevel
   * @returns
   * @memberof Logger
   */
  private logLevelToNumber(loglevel: LogLevel) {
    switch(loglevel) {
      case 'debug':
        return 0;
      case 'info':
      default:
        return 1;
      case 'error':
        return 2;
      case 'fatal':
        return 3;
    }
  }

  public debug(...content: any[]) {
    this.log(LogLevel.Debug, ...content);
  }
  public info(...content: any[]) {
    this.log(LogLevel.Info, ...content);
  }
  public error(...content: any[]) {
    this.log(LogLevel.Error, ...content);
  }
  public fatal(...content: any[]) {
    this.log(LogLevel.Fatal, ...content);
  }
  
  /**
   * Logs a debug log with function information.
   * 
   * @example
   * fndebug(["arg1", "arg2"], "test log", ["another test log is", 2]))
   * 
   * logs:
   *  function(arg1, arg2): test log
   *  function(arg1, arg2): another test log is 2
   *
   * @param {(any | any[])}   args  function arguments
   * @param {...any[]}        log   function log statement(s).
   *                                Multiple log statements will be
   *                                logged on separate lines (arrays will be expanded)
   */
  public fndebug(args?: any | any[], ...log: any[]): void {
    // immediately return if not debugging. This
    // prevents unnecessary stack loading which is
    // load intensive.
    if(this.logLevel > 0) return;

    let stack = this.getStack();
    let fnName = stack[2].getFunctionName();

    if(!fnName) {
      this.error('Could not get stack for fndebug()! complete stack:', new Error().stack)
    }

    // force array-ize the args
    let arg =
      Array.isArray(args) ?
        args : 
        args === undefined ?
          [ ] :
          [ args ];

    // map the arg array to a print format
    arg = arg.map(x => this.inspect(x));

    // define the printer
    function p(this: Logger, logLine?: any) {
      // convert the log to array, one-sized array or
      // empty array depending on logLine type
      let l: any[] = 
        Array.isArray(logLine) ? 
          logLine : 
          logLine === undefined 
            ? [] 
            : [ logLine ];
      
      this.debug(`${fnName}(${arg.join(', ')})${l.length > 0 ? ':' : ''}`, ...l);
    }
    
    if(log.length > 0) {
      log.forEach(p.bind(this));
    } else {
      p.bind(this)();
    }
  }

  /**
   * Starts, stops or logs an update on a benchmark.
   * If a name is not specified, this either starts or stops
   * a benchmark. If name is specified, this logs the current time
   * of the benchmark.
   * 
   * @example
   * logger.benchmark(); // starts the benchmark
   * ...stuff...
   * logger.benchmark('stage 1 done');
   * ...more stuff...
   * logger.benchmark(); // ends the benchmark
   *
   * @param {string} [name]
   * @memberof Logger
   */
  public benchmark(name?: string) {
    let now = Date.now();
    if(!name) {
      // start or stop the benchmark
      if(this.lastBenchmark) {
        this.debug('benchmarked', (now - this.lastBenchmark) + "ms");
        this.lastBenchmark = null;
      } else {
        this.lastBenchmark = Date.now();
      }
    } else if(this.lastBenchmark) {
      // do a check on log
      this.debug(name, 'in', (now - this.lastBenchmark) + "ms");
    }
  }

  /**
   * Wrapper for util.inspect(...)
   *
   * @param {*} object
   * @returns inspected object
   * @memberof Logger
   */
  public inspect(object: any) {
    return util.inspect(object, {
      colors: true,
      getters: true,
      compact: true,
    });
  }

  /**
   * Logs content using the specified level
   *
   * @param {LogLevel} [level=LogLevel.Info]
   * @param {...any[]} content
   * @returns
   * @memberof Logger
   */
  public log(level: LogLevel = LogLevel.Info, ...content: any[]) {
    if(this.logLevelToNumber(level) < this.logLevel) {
      return;
    }

    let item: LogItem = {
      content,
      level,
      outputs: this.outputs,
      date: new Date().toISOString(),
      module: this.name,
    };

    // loop all outputs
    for (const out of this.outputs) {
      this.log_to_output(item, out);
    }
  }

  /**
   * Gets the current stack trace. Note that this is
   * resource intensive and therefore should be used
   * while debugging only.
   *
   * @private
   * @returns { any[] } the callstack
   * @memberof Logger
   */
  private getStack(): any[] {
    let orig = Error.prepareStackTrace;
    Error.prepareStackTrace = function(_, stack) {
      return stack;
    };
    let err = new Error;
    Error.captureStackTrace(err);
    var stack = (err.stack as any);
    Error.prepareStackTrace = orig;
    return stack;
  }
  
  /**
   * logs a LogItem to a specific LogOutput
   *
   * @private
   * @param {LogItem}     item
   * @param {LogOutput}   output
   * @memberof Logger
   */
  private log_to_output(item: LogItem, output: LogOutput) {
    switch(output) {
      case LogOutput.Console:
        this.out_console(item);
        break;
      case LogOutput.File:
        throw new Error('LogOutput.File is not implemented');
    }
  }

  /**
   * Console endpoint
   *
   * @private
   * @param {LogItem}     item
   * @memberof Logger
   */
  private out_console(item: LogItem) {
    let color = (x: string) => x;
    const module_length = 7;
    const level_length  = 5;
    let debugTime = '';
    
    // append time since last  debug
    // message if we are indeed
    // debugging
    if(this.logLevel === 0) {
      if(this.lastLogTime !== 0) {
        let diff = Date.now() - this.lastLogTime;
        debugTime = colors.gray(' +' + timespan.parse(diff))
      }
    }

    this.lastLogTime = Date.now();

    let levelPad = '';
    let modulePad = '';

    // don't do unnecessary processing if we don't need to.
    // This pads the module and level columns nicely so the
    // actual log text is leveled
    if(moreReadableLogs) {
      item.level = item.level.substring(0, level_length) as LogLevel;
      item.module = item.module.substring(0, module_length);
      levelPad = ' '.repeat(level_length - item.level.length);
      modulePad = ' '.repeat(module_length - item.module.length);
    }

    if(item.level === LogLevel.Error) {
      item.level = colors.red(item.level) as LogLevel;
    }
    else if (item.level == LogLevel.Debug) {
      item.level = colors.blue(item.level) as LogLevel;
    }
    else if (item.level == LogLevel.Fatal) {
      color = colors.red;
    }
    
    console.log(color(format(LOG_FORMAT, {
      ...item,
      levelPad,
      modulePad,
      content: item.content.map((x: any) => {
        if(typeof(x) === 'string')
          return x;
        else return this.inspect(x);
      }).join(' '),
    })) + debugTime);
  }

}