import {  } from './types';
import * as vega from 'vega';
import * as canvas from 'canvas';
import fs from 'fs'
import { DbNodeData, DbVoiceChannelData, DbGuildData, DbSessionData } from './types';
import discord from 'discord.js';
import { getTempFilePath } from './helpers';

export default async function(data: DbNodeData[], guild: discord.Guild | DbGuildData, session: DbSessionData) {
  let max = 0;
  let min = 0;
  let average = 0;
  let onlines: number[] = []
  let time_min = new Date().valueOf();
  let time_max = 0;
  let name = guild.name;
  let vc_data: {
    [name: string]: {
      date: Date,
      amount: number,
      limit: number,
    }[]
  } = {};
  let vc_totals: {
    [date: number]: number
  } = {};
  let vc_avg = 0;
  let vc_max = 0;
  
  if(data.length > 0) {
    min = data[0].onlineUsers;
    max = data[0].onlineUsers;
  }
  data.forEach(s => {
    let online = s.onlineUsers;
    min = Math.min(min, online);
    max = Math.max(max, online);
    time_min = Math.min(time_min, s.timestamp.valueOf());
    time_max = Math.max(time_max, s.timestamp.valueOf());
    onlines.push(online);
    s.voiceChannels.forEach((vc: DbVoiceChannelData) => {
      if(vc.users >= 0) {
        if(!vc_data[vc.name]) {
          vc_data[vc.name] = [];
        }
        if(!vc_totals[s.timestamp.valueOf()])
          vc_totals[s.timestamp.valueOf()] = 0;
        vc_totals[s.timestamp.valueOf()] += vc.users;
        vc_data[vc.name].push({
          amount: vc.users,
          limit: vc.maxUsers || 0,
          date: s.timestamp
        })
      }
    })
  });

  let vc_data_arr = [];
  for (const vc_name in vc_data) {
    if (vc_data.hasOwnProperty(vc_name)) {
      const vc = vc_data[vc_name];
      vc_data_arr.push({
        name: vc_name,
        nodes: vc
      })
    }
  }

  let vc_totals_arr = [];
  for (const vc_date in vc_totals) {
    if (vc_totals.hasOwnProperty(vc_date)) {
      const vc = vc_totals[vc_date];
      vc_totals_arr.push({
        date: new Date(Number.parseInt(vc_date)),
        amount: vc
      });
      vc_max = Math.max(vc, vc_max);
    }
  }
  let _fvc = vc_totals_arr.map(x => x.amount).filter(x => x > 0);
  if(_fvc.length > 0)
    vc_avg = _fvc.reduce((a, b) => a + b) / _fvc.length;

  average = onlines.reduce((a, b) => a + b) / onlines.length;

  let description = vega.parse({
    "$schema": "https://vega.github.io/schema/vega/v5.json",
    "width": 750,
    "height": 500,
    "padding": 5,
    "background": "white",
    "title": {
      "text": "Server usage statistics for " + name,
      "subtitle": "Provided by the Discord Usage Stats bot" + 
                  `\n${session.started.toLocaleString()} - ${session.ended ? session.ended.toLocaleString() : ''}`,
      "orient": "right",
      "anchor": "end",
      "dy": 40,
      "fontSize": 13,
      "subtitleFontSize": 10,
    },

    "signals": [],
    "data": [
      {
        "name": "table",
        "values": data.map(x => ({
          online: x.onlineUsers, 
          time: x.timestamp, 
          color: "Total Online",
          strokeDash: true
        })),
        "format": {
          "parse": {'time': "date"}
        }
      },
      {
        "name": "average",
        "values": [
          {
            "online": average,
            "time": new Date(time_min),
            "color": "Average users online",
            "strokeDash": true
          },
          {
            "online": average,
            "time": new Date(time_max),
            "color": "Average users online",
            "strokeDash": true
          },
        ]
      },
      {
        "name": "total_vc",
        "values": vc_totals_arr.map(n => ({
          "online": n.amount,
          "time": n.date,
          "color": "Total users in voice channels",
          "strokeDash": false
        }))
      },
      ...vc_data_arr.map(x => ({
        "name": x.name,
        "values": x.nodes.map(n => ({
          "online": n.amount,
          "time": n.date,
          "color": x.name,
          "strokeDash": true
        }))
      }))
    ],
    "scales": [
      {
        "name": "color",
        "type": "ordinal",
        "domain": {
          "fields": [
            {"data": "table", "field": "color"},
            {"data": "average", "field": "color"},
            {"data": "total_vc", "field": "color"},
            ...vc_data_arr.map(x => ({
              "data": x.name,
              "field": "color"
            }))
          ]
        },
        "range": "category",
      },
      {
        "name": "strokeDash",
        "type": "ordinal",
        "range": [
          [1, 0],
          [8, 8],
        ],
        "domain": {
          "fields": [
            {"data": "table", "field": "strokeDash"},
            {"data": "total_vc", "field": "strokeDash"},
            {"data": "average", "field": "strokeDash"},
            ...vc_data_arr.map(x => ({
              "data": x.name,
              "field": "strokeDash"
            }))
          ]
        },
      },
      {
        "name": "xscale",
        "type": "time",
        "domain": {"data": "table", "field": "time"},
        "range": "width",
        "padding": 0,
        "round": true,
        "nice": false,
      },
      {
        "name": "yscale",
        "domain": {
          "fields": [
            {"data": "table", "field": "online"},
            {"data": "average", "field": "online"},
            {"data": "total_vc", "field": "online"},
            ...vc_data_arr.map(x => ({
              "data": x.name,
              "field": "online"
            }))
          ]
        },
        "nice": true,
        "domainMin": 0,
        "range": "height",
        "padding": 50,
        "zero": true,
      }
    ],
    "projections": [],
    "axes": [
      { 
        "title": "Time",
        "orient": "bottom",
        "scale": "xscale",
        "tickCount": 'hour',
        "tickMinStep": 1,
        "labelAngle": 0,
        "labelOverlap": true,
        "format": '%H:%M',
        "formatType": 'utc',
      },
      { 
        "title": "Users", 
        "orient": "left",
        "scale": "yscale",
        "tickCount": 10,
        "tickMinStep": 1,
        "grid": true,
      }
    ],
    "legends": [
      {
        "type": "symbol",
        "title": "Server usage",

        "cornerRadius": 3,
        "padding": 5,
        "strokeColor": "#ccc",

        "fill": "color",

        "values": [ "Total Online", "Average users online", "Total users in voice channels" ],
      },
      {
        "type": "symbol",
        "title": "Voice channel usage",

        "cornerRadius": 3,
        "padding": 5,
        "strokeColor": "#ccc",

        "fill": "color",

        "values": vc_data_arr.map(x => x.name),
      }
    ],
    "marks": [
      {
        "type": "line",
        "from": {"data":"average"},
        "encode": {
          "enter": {
            "stroke": {"scale": "color", "field": "color"},
            "strokeWidth": {"value": 3},
            "strokeDash": {"value": [7, 6]},
            "interpolate": {"value": 'linear'},
            "x": {"scale": "xscale", "field": "time"},
            "y": {"scale": "yscale", "field": "online"},
          }
        }
      },
      {
        "type": "line",
        "from": {"data":"table"},
        "encode": {
          "enter": {
            "stroke": {"scale": "color", "field": "color"},
            "strokeWidth": {"value": 4},
            "interpolate": {"value": 'linear'},
            "x": {"scale": "xscale", "field": "time"},
            "y": {"scale": "yscale", "field": "online"},
          }
        }
      },
      ...vc_data_arr.map(x => ({
        "type": "line",
        "from": {"data": x.name},
        "encode": {
          "enter": {
            "stroke": {"scale": "color", "field": "color"},
            "strokeWidth": {"value": 2.5},
            "strokeDash": {
              "scale": "strokeDash",
              "field": "strokeDash"
            },
            "interpolate": {"value": 'linear'},
            "x": {"scale": "xscale", "field": "time"},
            "y": {"scale": "yscale", "field": "online"},
          }
        }
      } as any)),
      {
        "type": "line",
        "from": {"data":"total_vc"},
        "encode": {
          "enter": {
            "stroke": {"scale": "color", "field": "color"},
            "strokeWidth": {"value": 1.6},
            "strokeDash": {
              "scale": "strokeDash",
              "field": "strokeDash"
            },
            "interpolate": {"value": 'linear'},
            "x": {"scale": "xscale", "field": "time"},
            "y": {"scale": "yscale", "field": "online"},
          }
        }
      },
    ]
  });

  let view = new vega.View(description);
  await view.run();
  let chart: canvas.Canvas = await view.toCanvas(1.3, {type: 'png'}) as unknown as canvas.Canvas;

  let context = chart.getContext('2d');
  context.save();

  let hash = require('crypto').createHash('md5').update(chart.toDataURL()).digest("hex")
  let path = getTempFilePath(hash + '.png');

  let promise = new Promise<any>(resolve => {
    const out = fs.createWriteStream(path, {
      autoClose: true,
      flags: 'w+',
    })
    const stream = chart.createPNGStream()
    stream.pipe(out)
    out.on('finish', () => {
      out.close();
      resolve({
        min, max, average, chart: path,
        voicechannels_average: vc_avg,
        voicechannels_max: vc_max,
        close: () => {
          if(fs.existsSync(path)) fs.unlinkSync(path);
        }
      });
    });
  });

  return promise
}
