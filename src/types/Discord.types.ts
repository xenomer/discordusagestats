export interface GuildSnapshot {
    date: Date,
    name: string,
    id: string,
    total: number,
    online: number,
    offline: number,
    voiceChannels: VoiceChannelSnapshot[],
}
export interface VoiceChannelSnapshot {
    name: string,
    userAmount: number,
    userLimit: number
}
export interface TimerInstance {
    scheduleId: string,
    startedAt: Date,
    interval: number,
}
export interface EmbedMessageOptions {
    style?: 'error' | 'info' | 'success' | 'question',
    timestamp?: boolean | number,
}
export type Contexts = {
    [guildplususer: string]: {
        [command: string]: boolean
    }
}