import { ObjectId } from "bson";

export interface DbServerData {
    _id: ObjectId;
    name: string,
    created: string,
    debug: boolean,
}
export interface DbGuildData {
    _id: string;
    name: string,
    created: string,
    service: {
        enabled: boolean,
        interval: number,
        hostServer: ObjectId | null,
        currentSession: ObjectId | null,
        cleanThreshold: number,
    }
    roles: {
        view: string[],
        manage: string[]
    },
    commandPrefix: string,
    serverOverride: string | null,
    sessions: {
        [_id: string]: {
            _id: ObjectId,
            name: string,
            started: Date
        }
    }
}
export interface DbSessionData {
    _id: ObjectId,
    guild: string,
    name: string,
    started: Date,
    ended: Date | null,
}
export interface DbNodeData {
    _id: ObjectId,
    timestamp: Date,
    session: ObjectId,
    guild: string,
    totalUsers: number,
    onlineUsers: number,
    voiceChannels: DbVoiceChannelData[],
}
export interface DbVoiceChannelData {
    name: string,
    maxUsers: number | null,
    users: number,
}