declare module 'in-command';
declare module 'timespan-parser';
declare module 'random-color' {
  interface Color {
    hexString(): string;
    rgbString(): string;
  }
  function randomColor(): Color;
  function randomColor(saturation: number, value: number): Color;
  export = randomColor;
}
declare module 'cache-base';