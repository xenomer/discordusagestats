export enum LogLevel {
    Debug = "debug",
    Info = "info",
    Error = "error",
    Fatal = "fatal"
}
export enum LogOutput {
    Console = "console",
    File = "file"
}
export interface LogItem {
    content: any[],
    outputs: LogOutput[],
    level: LogLevel,
    date: Date | string,
    module: string,
}